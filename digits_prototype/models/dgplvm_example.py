import GPy
from load_plotting import *
import GPy
from GPy.core.parameterization.priors import DGPLVM_KFDA
from GPy.core.parameterization.priors import DGPLVM
from GPy.core.parameterization.priors import Gaussian
from sklearn.preprocessing import label_binarize
from scipy.spatial.distance import cdist

# from dgplvm_test import DGPLVM
# change the original pb to pt to avoid conflict naming
import numpy as np, matplotlib.pyplot as pt
import string
from load_plotting import *
# interactive
pt.ion()

def prepare_data(filename):
    '''

    :param filename: to load the data
    :return: feature array, label array
    '''
    # data setup
    # digits = np.load('digits.npy')
    digits = np.load(filename)
    which = [0, 3, 6, 7, 8, 9]  # which digits to work on
    digits = digits[which, :, :, :]
    num_classes, num_samples, height, width = digits.shape
    # array of string array
    #get label string
    labels = np.array([[str(l)] * num_samples for l in which])
    # get full
    # resize N*D
    Y = np.reshape(digits, [num_classes * num_samples, -1])

    # hint .shape

    return Y, labels.flatten(), which


def bin_labels(labels, which):
    '''
    binarize flatten labels
    :param labels: flatten labels
    :param which: array of classes
    :return: full
    '''
    which = np.array(which).astype(labels.dtype)
    full = label_binarize(labels, classes = which)

    return full


def dgplvm_digits(max_iters = 1000, sigma2 = np.power(10.0, -4), optimize=True, verbose=1, visual=True):

    """

    note:
    prior was given
    GPy.core.parameterization.priors.DGPLVM_KFDA

    """
    # ------------------- prepare the data
    Y, labels, which = prepare_data('digits.npy')
    Yn = Y - Y.mean(0)

    # ------------------- choose kernel
    input_dim = 6
    kernel = GPy.kern.RBF(input_dim, ARD=True) + GPy.kern.Bias(6)
    # kernel += GPy.kern.White(input_dim) + GPy.kern.Bias(input_dim)

    # ------------------- simple GPLVM
    m = GPy.models.GPLVM(Y, input_dim, kernel=kernel)
    m.data_labels = labels
    m.optimize('scg', max_iters=max_iters, messages=verbose)

    # ------------------- gaussian prior GPLVM
    prior = Gaussian(0, 1)
    m1 = GPy.models.GPLVM(Yn, input_dim=input_dim, kernel=kernel)
    m1.data_labels = labels
    m1.X.priors = prior

    # -------------------  discriminative GPLVM
    # need binarized label
    binLabels = bin_labels(labels, which)
    priorD = DGPLVM(sigma2, binLabels, x_shape = Yn.shape)
    # priorD_KFDA = DGPLVM_KFDA(lambdaa, sigma2, binLabels, kernel, x_shape = Yn.shape)

    m2 = GPy.models.GPLVM(Yn, input_dim=input_dim, kernel=kernel)
    m2.data_labels = labels
    m2.X.priors = priorD

    models = np.array([m1, m2])
    [mx.optimize(messages=1, max_iters=max_iters) for mx in models]

    # -------------------  plotting
    f = m.plot_latent(labels)
    f1 = m1.plot_latent(labels)
    f2 = m2.plot_latent(labels)

    # m1.optimize(messages=1, max_f_eval=1000) # optimize for 1000 iterations
    # # optimizer
    # figs = [mx.plot_latent(labels.flatten()) for mx in models]
    f.set_title('GPLVM')
    f1.set_title('Guassian prior, max_iters = ' + str(max_iters))
    f2.set_title('Discriminative prior, max_iters = ' + str(max_iters))
    # figs[2].set_title('Discriminative prior with KFDA')

    if visual:
        visual_eval(m, 'GPLVM pointwise covariance')
        visual_eval(m1, 'Gaussian prior pointwise covariance')
        visual_eval(m2, 'Discriminative pointwise covariance')

    return m,m1,m2

#  problem: not called
def visual_eval(model, title):
    # print title
    fig = pt.figure()
    S = model.latent_mean
    S_sim = cdist(S,S)
    pt.pcolor(S_sim)
    pt.colorbar()
    pt.title(title)

    return fig

# %load_ext autoreload
# %autoreload