# Copyright (c) 2012 - 2014 the GPy Austhors (see AUTHORS.txt)
# Licensed under the BSD 3-clause license (see LICENSE.txt)
from GPy.models.gplvm import GPLVM
from GPy.core.parameterization import priors
from GPy.core.parameterization.priors import Gaussian

class DGPLVM(GPLVM):

    """
    Discriminative Gaussian Process Latent Variable Model
    (modified from GPy.models.dpgplvm.DPBayesianGPLVM)
    Gaussian Process Latent Variable Model with Discriminative prior
    generate prior from
    GPy.core.parameterization.priors.DGPLVM_KFDA

    """

    def __init__(self, Y, input_dim, X_prior, X=None, X_variance=None, init='PCA', num_inducing=10,
                 Z=None, kernel=None, inference_method=None, likelihood=None,
                 name='bayesian gplvm', mpi_comm=None, normalizer=None,
                 missing_data=False, stochastic=False, batchsize=1):
        super(DGPLVM,self).__init__(Y=Y, input_dim=input_dim, X=X, X_variance=X_variance, init=init, 					num_inducing=num_inducing, Z=Z, kernel=kernel, inference_method=inference_method, likelihood=likelihood, mpi_comm=mpi_comm, normalizer=normalizer, missing_data=missing_data, stochastic=stochastic, batchsize=batchsize, name='dp bayesian gplvm')
        self.X.mean.set_prior(X_prior)
        self.link_parameter(X_prior)

