import GPy
# change the original pb to pt to avoid conflict naming
import numpy as np, matplotlib.pyplot as pt
import string
from load_plotting import *
from setup_data import *

# interactive
pt.ion()


def BGPLVM_example(Y, labels):
    """
    BGPLVM with fixed kernel, fixed noise, fixed optimization
    :param Y: input data
    :param label: input labels
    :return: BGPLVM model
    """

    Yn = Y-Y.mean(0)
    input_dim = 1 # How many latent dimensions to use

    kernel = GPy.kern.RBF(input_dim,ARD=True) # ARD kernel
    kernel += GPy.kern.White(input_dim) + GPy.kern.Bias(input_dim)


    # Likelihood base class, used to defing p(y|f).
    # default: likelihood = Gaussian()

    m = GPy.models.BayesianGPLVM(Yn, input_dim=input_dim, kernel=kernel, num_inducing=15)

    # start noise is 1% of datanoise
    setattr(m, 'noise', Yn.var()/100)

    m.optimize('bfgs',messages=1, max_f_eval=10000, max_iters=10000)
    # Running L-BFGS-B (Scipy implementation) Code:
    # modified


    # plot_model(m, m['rbf_len'].argsort()[:2], labels.flatten())

    m.kern.rbf.lengthscale.__dict__.keys()
    rbf_len = m.kern.rbf.lengthscale
    plot_model(m, rbf_len.argsort()[:2], labels.flatten())

    return m

    #
    # <ipython-input-34-39f5fd2d0a82> in <module>()
    # ----> 1 plot_model(m, rbf_len.argsort()[:2], labels.flatten())
    #
    # C:\Users\zhuya\Desktop\EE494\GPy-devel\digits_dataset\load_plotting.pyc in plot_model(m, which_dims, labels)
    #      13         pass
    #      14     for i, lab in enumerate(ulabs):
    # ---> 15         ax.scatter(*X[labels==lab].T,marker='o',color=colors[i],label=lab)
    #      16         pass
    #      17     pass
    #
    # AttributeError: 'NormalPosterior' object has no attribute 'T'
