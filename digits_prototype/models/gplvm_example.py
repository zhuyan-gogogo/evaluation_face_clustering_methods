import GPy
# change the original pb to pt to avoid conflict naming
import numpy as np, matplotlib.pyplot as pt
import string
from load_plotting import *


# interactive
pt.ion()


def GPLVM_fixed(Y, labels):

    """
    GPLVM with fixed kernel, fixed noise, fixed optimization
    :param Y: input data
    :param label: input labels
    :return: GPLVM model
    """
    Yn = Y-Y.mean(0)

    input_dim = 2  # How many latent dimensions to use
    # constructkernl for latent layers
    kernel = GPy.kern.Linear(input_dim,ARD=True) # ARD kernel
    kernel += GPy.kern.White(input_dim) + GPy.kern.Bias(input_dim)


    # Likelihood base class, used to define p(y|f).
    # default: likelihood = Gaussian()

    m = GPy.models.GPLVM(Yn, input_dim=input_dim, kernel=kernel)

    # start noise is 1% of datanoise
    setattr(m, 'noise', Yn.var()/200)

    m.optimize(messages=1, max_f_eval=1000) # optimize for 1000 iterations
    # optimizer

    linear_variance = m.kern.linear.variances
    # lower dimensions
    # flatten the label
    plot_model(m, linear_variance.argsort()[-2:], labels)
    which = [int(i) for i in set(labels)]

    pb.legend(labels = which)
    pb.title("GPLVM solution to digits.npy dataset using linear kernel")

    return m