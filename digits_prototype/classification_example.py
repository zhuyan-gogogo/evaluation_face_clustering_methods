import GPy

default_seed = 10000

"""
Run a Gaussian process classification on the three phase oil data. The demonstration calls the basic GP classification model and uses EP to approximate the likelihood.
note: 2 class
"""

try:import pods

except ImportError:raise ImportWarning('Need pods for example datasets. See https://github.com/sods/ods, or pip install pods.')

num_inducing=50
max_iters=100
kernel=None
optimize=True
plot=True
try:
    import pods
except ImportError:
    raise ImportWarning('Need pods for example datasets. See https://github.com/sods/ods, or pip install pods.')
data = pods.datasets.oil()
X = data['X']
Xtest = data['Xtest']
Y = data['Y'][:, 0:1]
Ytest = data['Ytest'][:, 0:1]
Y[Y.flatten() == -1] = 0
Ytest[Ytest.flatten() == -1] = 0

# Create GP model
m = GPy.models.SparseGPClassification(X, Y, kernel=kernel, num_inducing=num_inducing)
m.Ytest = Ytest

# Contrain all parameters to be positive
# m.tie_params('.*len')
m['.*len'] = 10.

# Optimize
if optimize:
    m.optimize(messages=1)
print(m)

# Test
probs = m.predict(Xtest)[0]
GPy.util.classification.conf_matrix(probs, Ytest)