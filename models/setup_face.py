from __future__ import print_function

'''
%load_ext autoreload
%autoreload 2

to cluster openface features
read in file containing the models and run clustering and save in csv file

'''

from dgplvm_eval import *
from process_data import *
from cluster_example import *

import pickle
import pandas as pd
from copy import deepcopy

import numpy as np
from dgplvm_eval import *
from cluster_example import *
from collections import Counter

import pickle

# read in models
plt.ion()


# ---- ---- ---- ---- ---- ----- ----- ------------ file start here
# ---- ---- ---- ----- get face data
filename = '../features/openface_files_6000.txt'
# featMat, labels, which = prepare_face(filename)
#
featMat, labels, which = prepare_face(filename,
                                      minSize=10, duplicate=False, mu=-1, distribution ='weibull')

featMat, labels, which = prepare_face(filename, mu=-1, distribution='weibull', seed=1)
# run_openface_models_random([sys.argv[1], outfile], seed=i, mu=-1, distribution='weibull')

# about the same
# mu=100, sigma=50, distribution='exponential'
# randomly generated face with total: 694, average: 69.40000, std: 68.12958
# std slightly below
# [129, 9, 58, 48, 148, 136, 178, 21, 87, 10]
# mu=100, sigma=50, distribution='random'
# randomly generated face with total: 824, average: 82.40000, std: 58.89516
# std moderately over
# mu=50, sigma=300, distribution='normal'
# [6, 1, 16, 260, 11, 399, 149, 30, 15, 84]
# randomly generated face with total: 971, average: 97.10000, std: 127.79394
# std significantly below -> compact
# mu=1, sigma=5, distribution='weibull'
#[100, 47, 79, 75, 105, 102, 117, 61, 88, 49]
# randomly generated face with total: 823, average: 82.30000, std: 23.07834
# std slightly over
# mu=1, sigma=0.8, distribution='weibull'
# randomly generated face with total: 727, average: 72.70000, std: 82.78412


counter = Counter(labels)
cnt = [counter[key] for key in np.unique(labels)]
plt.hist(cnt)

plt.show()