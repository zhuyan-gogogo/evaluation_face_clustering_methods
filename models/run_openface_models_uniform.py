from __future__ import print_function

'''
%load_ext autoreload
%autoreload 2
'''

from dgplvm_eval import *
from process_data import *
from cluster_example import *

import pickle
import pandas as pd
import copy

#get features
#filename = 'feature_path.txt'
#faceVec, labels = face_pixels(filename, down=0.5)
'''
to build feature dictionary properly, the file containing the paths to mat file should be put in root directory
eg.
features/openface_files.txt
features/openface/*.mat
'''

def face_models_uniform(argvs, minSize =0, duplicate = False, mu = 0, sigma=0, distribution = None):
    '''
    :param argvs: list of 2 string, input file name, output file name
    :param duplicate: whether to keep only one side view
    :param min: number of face
    :return: filenmame for the output
    '''

    filename = argvs[0]
    featMat, labels, which = prepare_face(filename,minSize, duplicate, mu, sigma, distribution)
    # merge = []
    # # for i in labels:
    # #     merge = merge + i
    #
    # labels = np.asarray(merge)

    # to select
    # ---- ---- test
    # max_iters = 1
    # sigmaOption = [np.power(10.0,j) for j in [-7, -4]]
    # dimOption = range(len(which) - 1, len(which)+1) #2
    # clusterOption = range(len(which) / 2, len(which) +1) #
    # knnOption = [8,10,12]

    max_iters = 1500

    sigmaOption = [np.power(10.0,j) for j in [-7, -4, -1, 1, 4]]
    dimOption = range(len(which) - 1, len(which)+len(which)/2)
    clusterOption = range(len(which) / 2, len(which) * 2)
    knnOption = [8,10,12,15]

    ss, dd = np.meshgrid(sigmaOption, dimOption)
    ss = ss.flatten()
    dd = dd.flatten()

    # ------- -------- -------- protected
    rootParamList = []
    secParamList = []
    nmiList = []
    amiList = []
    mList = []

    # build first model as raw
    # first is the raw
    m1 = build_model(featMat, labels, input_dim=featMat.shape[1], visual=False, max_iters=0, model='gplvm')
    m1.labels = labels
    m1.latent_mean = featMat
    mList.append(copy.deepcopy(m1))

    for n in dimOption:
        # list of models, newed for clustering
        sub = []
        headParam =[]


        for s in sigmaOption:
            param = "sigma2 = %.8f; input_dim = %d" % (s, n)
            print (" ----- running DGPLVM uniform prior: " + param)
            m3 = build_model(featMat, labels, sigma2=s, input_dim=n, visual=False, max_iters=max_iters, model='uniform')
            mList.append(copy.deepcopy(m3))
            sub.append(copy.deepcopy(m3))
            headParam.append(['uniform', n, s])
            # headParam.append(param)

            del m3


    # ---- ----- ----- ---- ---- save to files

    if len(argvs) > 1:
        outfile = argvs[1]
    else:
        PIK_suff = '_%diter.dat' % max_iters
        outfile = filename.replace('.txt',PIK_suff)

    with open(outfile, "wb") as f:
        pickle.dump(mList, f)

    # with open(PIK, "rb") as f:
    #     print pickle.load(f)
    return outfile


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print ('run_openface_models.py <inputfile> <outputfile>')
        sys.exit(2)

    elif len(sys.argv) == 2:
        face_models_uniform([sys.argv[1],'../experiments/openface_models_ss_1500i.dat'],
                    minSize=200, mu=1, sigma=5,distribution='weibull')
        face_models_uniform([sys.argv[1],'../experiments/openface_models_s_1500i.dat'],
                    minSize=200, mu=100, sigma=50,distribution='random')
        face_models_uniform([sys.argv[1],'../experiments/openface_models_m_1500i.dat'],
                    minSize=200, mu=100, sigma=50,distribution='exponential')
        face_models_uniform([sys.argv[1],'../experiments/openface_models_l_1500i.dat'],
                    minSize=200, mu=1, sigma=0.8,distribution='weibull')
        face_models_uniform([sys.argv[1],'../experiments/openface_models_ll_1500i.dat'],
                    minSize=200, mu=50, sigma=300,distribution='normal')
    else:
        face_models_uniform(sys.argv[1:])
