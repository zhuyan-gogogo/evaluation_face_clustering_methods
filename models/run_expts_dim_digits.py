'''
%load_ext autoreload
%autoreload 2
'''

from dgplvm_eval import *
from process_data import *
from cluster_example import *

import pickle
import pandas as pd
from copy import deepcopy

import numpy as np
from dgplvm_eval import *
from cluster_example import *

# df.set_index(['Form', 'nr Element', 'Type', 'Test']) will combine same entry

Y, labels, which = prepare_digits('../features/digits.npy')

print 'Labels are: ', which

# -------- --------- run single model on digits with default setting
# m = build_model(Y, labels, input_dim=6, sigma2=np.power(10.0, 4), model='gaussian')
# kmean_clustering(m.latent_mean, labels, n_clusters=6)
# spectral_clustering(m.latent_mean, labels, n_clusters=6, similarity='knn')


# to select
# sigmaOption = [np.power(10.0,j) for j in [-7, -4]]
# dimOption = range(len(which)-1, len(which))

sigmaOption = [np.power(10.0,j) for j in [-7, -4, -1, 1, 4, 5]]
dimOption = range(len(which)-1, len(which)*2)
clusterOption = range(len(which) / 2, len(which) * 2)

ss, dd = np.meshgrid(sigmaOption, dimOption)
ss = ss.flatten()
dd = dd.flatten()

# mList=[]
# clusterList= []

max_iters = 1500

# # ------- -------- -------- protected
rootParamList = []
secParamList = []
nmiList = []
amiList = []
mList = []

for n in dimOption:
    # list of models, newed for clustering
    sub = []
    headParam =[]
    param = "input_dim = %d" % (n)
    print (" ----- running GPLVM " + param)
    m1 = build_model(Y, labels, input_dim = n, visual=False, max_iters=max_iters, model='gplvm')
    mList.append(deepcopy(m1))
    sub.append(deepcopy(m1))
    headParam.append(['GPLVM', n, 0.0])
    # headParam.append(param)

    param = "input_dim = %d" % (n)
    print (" ----- running DGPLVM uniform prior: " + param)
    m2 = build_model(Y, labels, input_dim=n, visual=False, max_iters=max_iters, model='uniform')
    mList.append(deepcopy(m2))
    sub.append(deepcopy(m2))
    headParam.append(['uniform', n, 0.0])
    # headParam.append(param)

    for s in sigmaOption:
        param = "sigma2 = %.8f; input_dim = %d" % (s, n)
        print (" ----- running DGPLVM gaussian prior: " + param)
        m3 = build_model(Y, labels, sigma2=s, input_dim=n, visual=False, max_iters=max_iters, model='gaussian')
        mList.append(deepcopy(m3))
        sub.append(deepcopy(m3))
        headParam.append(['gaussian', n, s])
        # headParam.append(param)

        del m3

    # -------- --------- clustering

    print " ----- running clustering  ------"

    for nn in clusterOption:
        param = "n_cluster = %d" % nn
        print (" ----- running K mean clustering, dimension = %d" %(nn))
        kres = [kmean_clustering(m.latent_mean, labels, n_clusters= nn) for m in sub]
        # print kNMI
        # clusterList.append(np.asarray([nmi, ami, 'kmean']))
        rootParamList += headParam
        subParam = [['kmean', nn] for p in headParam]
        secParamList += subParam
        nmiList += [k[0] for k in kres]
        amiList += [k[1] for k in kres]

        print (" ----- running spectral clustering with nearsest neighbor, dimension = %d" % (nn))
        sres = [spectral_clustering(m.latent_mean, labels, n_clusters=nn, visual=False) for m in sub]
        rootParamList += headParam
        subParam = [['spectral', nn] for p in headParam]
        secParamList += subParam
        nmiList += [k[0] for k in sres]
        amiList += [k[1] for k in sres]

        # print sNMI
        # clusterList.append(np.asarray([nmi, ami, 'spectral knn']))
        # nmi, ami = spectral_clustering(m.latent_mean, labels, n_clusters=nn, visual=False)
        # clusterList.append(np.asarray([nmi, ami, 'spectral cdist']))

        print (" ----- running affinity propagation with squared correlation")
        ares = [ap_clustering(m.latent_mean, labels=labels, affinity='square_correlation', visual=False) for m in sub]
        # print apNMI
        rootParamList += headParam
        subParam = [['ap', nn] for p in headParam]
        secParamList += subParam
        nmiList += [k[0] for k in ares]
        amiList += [k[1] for k in ares]

# ---- ----- ----- ---- ---- without models
# max_iters = 0
# for nn in clusterOption:
#     param = "n_cluster = %d" % nn
#     print (" ----- running K mean clustering, dimension = %d" %(nn))
#     kres = kmean_clustering(Y, labels, n_clusters= nn)
#     # print kNMI
#     # clusterList.append(np.asarray([nmi, ami, 'kmean']))
#     secParamList.append(['kmean', nn])
#     nmiList.append(kres[0])
#     amiList.append(kres[1])
#
#     print (" ----- running spectral clustering with nearsest neighbor, dimension = %d" % (nn))
#     sres = spectral_clustering(Y, labels, n_clusters=nn, similarity='knn', visual=False)
#     secParamList.append(['spectral', nn])
#     nmiList.append(sres[0])
#     amiList.append(sres[1])
#
#     # print sNMI
#     # clusterList.append(np.asarray([nmi, ami, 'spectral knn']))
#     # nmi, ami = spectral_clustering(m.latent_mean, labels, n_clusters=nn, visual=False)
#     # clusterList.append(np.asarray([nmi, ami, 'spectral cdist']))
#
#     print (" ----- running affinity propagation with squared correlation")
#     ares = ap_clustering(Y, labels=labels, affinity='square_correlation', visual=False)
#     # print apNMI
#     secParamList.append(['ap', nn])
#     nmiList.append(ares[0])
#     amiList.append(ares[1])
#
# rootParamList = secParamList

# ---- ----- ----- ---- ---- save to files

PIK = '../experiments/digits_model_%diter.dat' % max_iters

with open(PIK, "wb") as f:
    pickle.dump(mList, f)

# with open(PIK, "rb") as f:
#     print pickle.load(f)

# save to csv
evalDict = {'model params': rootParamList, 'cluster param': secParamList, 'nmi': nmiList, 'ami': amiList}
# fo = 'digits_nmi_%diter.npy' % max_iters
# np.save(fo, evalDict)

df = pd.DataFrame(evalDict)
CSV = '../experiments/digits_model_%diter_re.csv' % max_iters
df.to_csv(CSV, encoding='utf-8')
