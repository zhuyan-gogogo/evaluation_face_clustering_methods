from __future__ import print_function


import numpy as np
import matplotlib.pyplot as plt

from sklearn.decomposition import KernelPCA

'''
%load_ext autoreload
%autoreload 2
'''
from dgplvm_eval import *
from process_data import *
from cluster_example import *

import pickle
import pandas as pd
import copy

#get features
#filename = 'feature_path.txt'
#faceVec, labels = face_pixels(filename, down=0.5)
'''
to build feature dictionary properly, the file containing the paths to mat file should be put in root directory
eg.
features/openface_files.txt
features/openface/*.mat
'''

filename = '../features/openface_files.txt'
# featMat, labels, which = prepare_face(filename)
#
featMat, labels, which = prepare_face(filename,
                                      minSize=0, duplicate=False,
                                      mu=0, sigma=0.5, distribution='weibull')

# Y, labels, which = prepare_digits('../features/digits.npy')

kpca = KernelPCA(kernel="rbf", fit_inverse_transform=True, gamma=10, n_components=200)
X_kpca = kpca.fit_transform(featMat)
X_back = kpca.inverse_transform(X_kpca)
