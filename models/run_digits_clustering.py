from __future__ import print_function

'''
%load_ext autoreload
%autoreload 2

to cluster openface features
read in file containing the models and run clustering and save in csv file

'''

from dgplvm_eval import *
from process_data import *
from cluster_example import *

import pickle
import pandas as pd
from copy import deepcopy

import numpy as np
from dgplvm_eval import *
from cluster_example import *

import pickle
# read in models


# ---- ---- ---- ---- ---- ----- ----- ------------ file start here
# ---- ---- ---- ----- get face data

Y, labels, which = prepare_digits('../features/digits.npy')

# ---- ---- ---- ----- selection of parameters
sigmaOption = [np.power(10.0,j) for j in [-7, -4, -1, 1, 4]]
dimOption = range(len(which) - 1, len(which)+len(which)/2)

clusterOption = range(len(which) / 2, len(which) * 2)
# knnOption = [8,10,12,15]

# #  toy
# sigmaOption = [np.power(10.0,j) for j in [-7, -4]]
# dimOption = range(len(which) - 1, len(which)+1)
#
# clusterOption = range(len(which) / 2, len(which)+1)
# knnOption = [8,10,12]

# ---- ---- ---- ----- cluster function
def get_clustering(argvs):
    infile = argvs[0]
    outfile = argvs[1]

    file = open(infile, 'rb')
    models = pickle.load(file)
    file.close()

    modelParamList = []
    clusterParamList = []
    nmiList = []
    amiList = []

    # ---- ---- ---- ---- model parameter
    for i in range(len(models)):
        modelParam = []
        m = models[i]
        priorName = m.prior_name

        # name
        if priorName == 'None':
            modelParam.append('GPLVM')
        else:
            modelParam.append(priorName)

        # latent dim
        modelParam.append(m.latent_mean.shape[1])
        # sigma
        if priorName == 'Gaussian':
            sigma = sigmaOption[(i-2)%len(sigmaOption)]
            modelParam.append(sigma)

        # ---- ----- ----- ----- clustering for each model
        print (" ----- running clustering  ------")
        print (modelParam)

        print(" ----- running affinity propagation with squared correlation")
        # ares = ap_clustering(m.latent_mean, labels=labels, visual=False)
        ares = ap_clustering(m.latent_mean, labels=labels, affinity='square_correlation', visual=False)
        # print apNMI
        modelParamList.append(modelParam)
        clusterParam = ['ap']
        clusterParamList.append(clusterParam)
        nmiList.append(ares[0])
        amiList.append(ares[1])


        for nn in clusterOption:
            print (" ----- running K mean clustering, dimension = %d" % (nn))
            kres = kmean_clustering(m.latent_mean, labels, n_clusters=nn)
            # print kNMI
            # clusterList.append(np.asarray([nmi, ami, 'kmean']))
            modelParamList.append(modelParam)
            clusterParam = ['kmean', nn]
            clusterParamList.append(clusterParam)
            nmiList.append(kres[0])
            amiList.append(kres[1])

            print(" ----- running spectral clustering with nearsest neighbor, dimension = %d" % (
            nn))
            sres = spectral_clustering(m.latent_mean, labels, n_clusters=nn, similarity='knn',visual=False)
            modelParamList.append(modelParam)
            clusterParam = ['spectral', nn]
            clusterParamList.append(clusterParam)
            nmiList.append(sres[0])
            amiList.append(sres[1])

            # print sNMI
            # clusterList.append(np.asarray([nmi, ami, 'spectral knn']))
            # nmi, ami = spectral_clustering(m.latent_mean, labels, n_clusters=nn, visual=False)
            # clusterList.append(np.asarray([nmi, ami, 'spectral cdist']))



    # save to csv
    evalDict = {'model params': modelParamList, 'cluster param': clusterParamList, 'nmi': nmiList, 'ami': amiList}
    # fo = 'digits_nmi_%diter.npy' % max_iters
    # np.save(fo, evalDict)

    df = pd.DataFrame(evalDict)
    df.to_csv(outfile, encoding='utf-8')

    return evalDict


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print ('run_digits_clustering.py <inputfile> <outputfile.csv>')
        sys.exit(2)

    evalDict = get_clustering(sys.argv[1:])