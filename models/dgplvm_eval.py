from scipy.spatial.distance import cdist

import GPy
from GPy.core.parameterization.priors import DGPLVM
from GPy.core.parameterization.priors import DGPLVM_KFDA
from GPy.core.parameterization.priors import Gaussian
from GPy.core.parameterization.priors import Uniform
from process_data import *
from cluster_example import *


def build_model(Y, labels, max_iters=1000, input_dim = 10, sigma2=np.power(10.0, -4), lambdaa = 1, visual=False, model='gplvm'):
    """
    :param Y: feature matrix
    :param labels: array
    :param max_iters:
    :param sigma2: for DGPLVM
    :param for dgplvm-KDFA only
    :param visual: for covariance matrix
    :param model: 'gplvm', 'gaussian', 'uniform','dgplvm'

    :return: model
    note:
    prior was given
    GPy.core.parameterization.priors.DGPLVM_KFDA
    """
    # ------------------- prepare the data
    # array of possible observations
    which = np.unique(labels)
    # do not need to normalize features
    Yn = Y
    # need binarized label
    binLabels = bin_labels(labels, which)

    # ------------------- choose kernel

    kernel = GPy.kern.RBF(input_dim, ARD=True)
    # kernel += GPy.kern.White(input_dim) + GPy.kern.Bias(input_dim)

    # ------------------- simple GPLVM
    m = None
    if model == 'gplvm':
        m = GPy.models.GPLVM(Y, input_dim, kernel=kernel)
        m.data_labels = labels
        optimize = True
        verbose = 1
        m.prior_name = 'None'
        m.optimize('scg', max_iters=max_iters, messages=verbose)

    # models =[]

    elif model == 'gaussian':
        # ------------------- gaussian prior GPLVM
        prior = Gaussian(0, sigma2)
        m = GPy.models.GPLVM(Yn, input_dim=input_dim, kernel=kernel)
        # m.data_labels = labels
        m.X.priors = prior
        m.prior_name = 'Gaussian'
        m.optimize(messages=1, max_iters=max_iters)
        # models.append(m)

    elif model == 'dgplvm':
        # -------------------  discriminative GPLVM
        priorD = DGPLVM(sigma2, binLabels, x_shape=Yn.shape)
        m = GPy.models.GPLVM(Yn, input_dim=input_dim, kernel=kernel)
        # m.data_labels = labels
        m.X.priors = priorD
        m.prior_name = 'Discriminative'
        m.optimize(messages=1, max_iters=max_iters)

        # models.append(m)

    elif model == 'uniform':
        # ------------------- uniform prior GPLVM
        priorU = Uniform(0, 1)
        m = GPy.models.GPLVM(Yn, input_dim=input_dim, kernel=kernel)
        # m.data_labels = labels
        m.X.priors = priorU
        m.prior_name = 'Uniform'
        m.optimize(messages=1, max_iters=max_iters)
        # models.append(m)

    elif model == 'kfda':
        # ------------------- dpglvm with kfda
        # regulariztion param
        lambdaa = np.power(10.0, -8)
        priorD_KFDA = DGPLVM_KFDA(lambdaa, sigma2, binLabels, kernel, x_shape = Yn.shape)
        m = GPy.models.GPLVM(Yn, input_dim=input_dim, kernel=kernel)
        m.data_labels = labels
        m.X.priors = priorD_KFDA
        m.prior_name = ' with KFDA'
        m.optimize(messages=1, max_iters=max_iters)
        # models.append(m)

    # max_iters=5
    # m.optimize(messages=1, max_iters=max_iters)
    # figs = []

    if visual:
        fig = m.plot_latent(labels)
        title = m.name + ' Prior:' + m.prior_name + ' max_iters = ' + str(max_iters)
        fig.set_title(title)
        visual_eval(model, 'Prior: ' + m.prior_name)

    return m


#  problem: not called
def visual_eval(model, title):
    # print title
    fig = pt.figure()
    S = model.latent_mean
    S_sim = cdist(S, S)
    pt.pcolor(S_sim)
    pt.colorbar()
    pt.title(title)

    return fig

    # %load_ext autoreload
    # %autoreload
    # mod
