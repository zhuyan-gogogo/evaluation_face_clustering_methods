from sklearn.preprocessing import label_binarize
import numpy as np, matplotlib.pyplot as pt
import numpy.random as rand
import random
import cv2

import scipy.io


def build_dict(filename):
    '''
    for now, only support .mat file
    :param filename:
    :param faceDirectory
    :return: python dictioinal
    '''

    f = open(filename, 'r')
    files= [line.rstrip('\n') for line in f]
    featDict = {}

    rootDir = filename.split('/')[:-1]


    labelShape = []

    for i in files:
        # for windows systems
        if i.startswith('/c/'):
            i = i.replace('/c/', 'C:/')
        mat = scipy.io.loadmat(i)
        i = i.split('/')
        # index = i.index(rootDir[-1])
        # path = rootDir + i[index+1:]
        # mat = scipy.io.loadmat('/'.join(path))
        # get label
        label = i[-1].split('_')[0]
        featDict[label] = mat['features']
        labelShape.append(featDict[label].shape)

    # print labelShape

    return featDict


def prepare_face(filename, minSize = 0, duplicate = False, mu = 0, sigma = 0, distribution = None, seed = 42):
    '''

    :param filename:
    :param minSize: remove small face dataset for training purpose
    :param duplicate: whether to remove duplicate
    :param mu: if 0, then no random choice,
    :param sigma:
    :param distribution: {}
    :return: featMat, labels, which
    '''
    random.seed(seed)

    faceFeat = build_dict(filename)
    faceDict = {}
    # transpose and cut from min
    for key in faceFeat:
        if faceFeat[key].shape[1] >= minSize:
            faceDict[key] = faceFeat[key].T
            # print faceDict[key].shape

    if not duplicate:
       for key in faceDict:
           faceDict[key] = faceDict[key][::2]

    if mu is not 0:
        faceDict = pick_random_from_identity(faceDict, mu, sigma, distribution, seed)

    labels = np.array([[key] * faceDict[key].shape[0] for key in faceDict])
    which = faceDict.keys()
    featMat = [faceDict[key] for key in faceDict]
    featMat = np.vstack(tuple(featMat))

    eachsize = [faceFeat[key].shape[1] for key in faceFeat]

    merge = []
    for i in labels:
        merge +=list(i)

    labels = np.asarray(merge)

    return featMat, labels, which


def pick_random_from_identity(faceDict, mu =100 , sigma=1, distribution = None, seed =42):
    '''
    get average of 100 faces for 10 identity,
    choices of distribution
    :param featDict:
    :param mu:
    :param sigma:
    :param distribution: {None, normal, exponential, fixed, random}
    :return: faceDict
    '''

    if distribution is None:
        randomChoice = np.random.choice(faceDict.keys(), 10, replace=False)
    elif distribution is 'weibull':
        randomWeight = np.random.weibull(seed/25.0, size=len(faceDict.keys()))
        # sum to 1
        randomWeight = randomWeight / sum(randomWeight)
        randomChoice = np.random.choice(faceDict.keys(), 10,replace=False,p=randomWeight)
    # randomSubChoice = np.random.choice()
    track = []
    tempDict = {}
    for key in randomChoice:
        rand.shuffle(faceDict[key])
        tempDict[key] = faceDict[key]
        track.append(tempDict[key].shape[0])

    # randomly sampled identities
    if mu == -1:
        print (track)
        # checkSize = []

        print ('randomly generated face with total: %d, average: %.5f, std: %.5f'
               % (np.sum(np.abs(track)), np.mean(track), np.std(track)))

        return tempDict

    # uniform distribution


    if distribution is None:
        track = []
        for key in tempDict:
            randInt = np.abs(rand.randint(mu-sigma, mu+sigma))+1
            # std ~ 50
            track.append(randInt)
            tempDict[key] = tempDict[key][:randInt]


    elif distribution is 'normal':
        track = []
        for key in tempDict:
            randInt = np.abs(int(random.gauss(mu=mu, sigma=sigma))) % tempDict[key].shape[0]
            track.append(randInt)
            tempDict[key] = tempDict[key][:np.abs(randInt)]
        # print ('randomly generated face with average: %d, std: %d'
        #        %(np.mean(track), np.std(track)))

    elif distribution is 'weibull':
        track = []
        for key in tempDict:
            randInt = np.abs(int(random.weibullvariate(alpha=mu, beta=sigma)*100))% tempDict[key].shape[0]
            track.append(randInt)
            tempDict[key] = tempDict[key][:np.abs(randInt)]
        # print ('randomly generated face with average: %d, std: %d'
        #        %(np.mean(track), np.std(track)))


    elif distribution is 'exponential':
        track = []
        for key in tempDict:
            randInt = np.abs(int(random.expovariate(lambd = 1.0/mu)))+1
            track.append(randInt)
            tempDict[key] = tempDict[key][:np.abs(randInt)]
        # print ('randomly generated face with average: %d, std: %d'
        #        %(np.mean(track), np.std(track)))
    #     randomly generated face with average: 1070, std: 832


    elif distribution is 'fixed':
        track = []
        for key in randomChoice:
            tempDict[key] = tempDict[key][:mu]
            track.append(mu)

    elif distribution is 'uniform':
        # always around the same average and std
        track = []
        for key in tempDict:
            randInt = np.abs(int(random.choice(range(5, 200, 1))))+1
            track.append(randInt)
            tempDict[key] = tempDict[key][:np.abs(randInt)]

        # print ('randomly generated face with average: %d, std: %d' % (np.mean(track), np.std(track)))


    print (track)
    # checkSize = []

    print ('randomly generated face with total: %d, average: %.5f, std: %.5f'
           % (np.sum(np.abs(track)), np.mean(track), np.std(track)))

    return tempDict


def face_pixels(filename, down = 0, directory = None):
    '''
    from text file, get features for all listed image
    :param filename: can be full name or from current dir
    :param down: downsample
    :return: features matrix, label
    '''

    f = open(filename, 'r')
    imgDict = [line.rstrip('\n') for line in f]
    imgSubPath = ['/'.join(line.split('/')[-3:]) for line in imgDict]

    if directory is not None:
        imgSubPath = [directory + '/'.join(line.split('/')[-2:]) for line in imgDict]
    else:
        # full directory provided in textfile
        imgSubPath = ['/'.join(line.split('/')[-3:]) for line in imgDict]


    labels = [line.split('/')[-2] for line in imgSubPath]
    features =[]

    for path in imgSubPath:
        img = cv2.imread(path, 0)
        features.append(img)

    if down is not 0:
        features = [cv2.resize(single, (0,0), fx = down, fy = down)
                    for single in features]

    f.close()

    return np.asarray([feat.flatten() for feat in features]), np.asarray(labels)


def prepare_digits(filename):
    '''

    :param filename: to load the data
    :return: feature array, label array
    '''
    # data setup
    # digits = np.load('digits.npy')
    digits = np.load(filename)
    which = [0, 3, 6, 7, 8, 9]  # which digits to work on
    digits = digits[which, :, :, :]
    num_classes, num_samples, height, width = digits.shape
    # array of string array
    # get label string
    labels = np.array([[str(l)] * num_samples for l in which])
    # get full
    # resize N*D
    Y = np.reshape(digits, [num_classes * num_samples, -1])

    # hint .shape

    return Y-np.mean(Y), labels.flatten(), which



def bin_labels(labels, which):
    '''
    binarize flatten labels
    :param labels: flatten labels
    :param which: array of classes
    :return: full
    '''
    which = np.array(which).astype(labels.dtype)
    full = label_binarize(labels, classes=which)

    return full
