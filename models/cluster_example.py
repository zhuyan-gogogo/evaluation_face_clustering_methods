# use three different clusterimport time
import time
import numpy as np
import matplotlib.pyplot as pt
from matplotlib.pyplot import cm
from sklearn.metrics.pairwise import pairwise_distances_argmin
from sklearn import metrics
from sklearn.preprocessing import scale
from sklearn.cluster import KMeans, SpectralClustering, AffinityPropagation
from scipy.spatial.distance import cdist
from sklearn.metrics.cluster.supervised import contingency_matrix
from pylab import *

# from scipy.spatial.distance import cdist

# def plot_cluster(model):
#     fig = pt.figure(figsize=(8, 3))
#     # fig.subplots_adjust(left=0.02, right=0.98, bottom=0.05, top=0.9)
#     # ax = fig.add_subplot(1, 3, 1)

def clustering_default(features, labels = [], methods = ['kmean'], select = False, visual = False):
    '''
    wrapper to run the clustering method
    :param model:for GP models
    :param methods: {kmean, spectral, ap}
    :param select:
    :return:
    '''
    # if select:
    #     features = select_dimension(model)
    # else:
    #     features = model.latent_mean
    #
    # labels = model.data_labels

    for method in methods:
        if (method == 'kmean'):
            clusters = kmean_clustering(features, labels,True)
        elif (method == 'spectral'):
            clusters = spectral_clustering(features, labels, 'knn')
        # elif (method == 'ap'):
        #     print

    return clusters


def kmean_clustering(features, labels = [], auto =True, n_clusters=6, max_iter=300,visual = False):
    '''

    :param features:
    :param labels: ground true labels, if supervised
    :param auto: use predict(X) to get the labels
    :return: kmean clusters
    :param visual
    '''

    # get number of clusters from label
    X = scale(features)
    # ---- supervised
    if labels is not []:
        cluster_label = np.unique(labels)
        n_clusters = len(cluster_label)
        # labels = model.data_labels

    from sklearn.utils import shuffle
    X, labels = shuffle(X, labels, random_state = 0)


    k_means = KMeans(init='k-means++', n_clusters = n_clusters, n_init=10, max_iter=max_iter)
    t0 = time.time()
    k_means.fit(X)
    t_batch = time.time() - t0
    # can also consider minibatch clustering for higher efficiency

    k_means_cluster_centers = k_means.cluster_centers_
    # ---- get labels form KMeans
    if auto:
        k_means_labels = k_means.predict(X)
    else:
        k_means_labels = pairwise_distances_argmin(X, k_means_cluster_centers)


    if (visual):

        fig = pt.figure()
        ax = fig.add_subplot(111)
        color = cm.rainbow(np.linspace(0, 1, n_clusters))

        for k, col in zip(range(n_clusters), color):
            my_members = k_means_labels == k
            cluster_center = k_means_cluster_centers[k]
            ax.plot(X[my_members, 0], X[my_members, 1], 'w',
                    markerfacecolor=col, marker='o')
            ax.plot(cluster_center[0], cluster_center[1], 'x', markerfacecolor=col,
                    markeredgecolor=col, markersize=10)

        ax.set_title('KMeans')
        ax.set_xticks(())
        ax.set_yticks(())
        pt.axis()
        pt.text(-2.5,2, 'train time: %.2fs\ninertia: %f' % (
            t_batch, k_means.inertia_))

    nmi = metrics.normalized_mutual_info_score(labels, k_means_labels)
    ami = metrics.adjusted_mutual_info_score(labels, k_means_labels)

    print "mutual information score: adjusted - %0.8f, normalized - %0.8f" %(ami, nmi)
    return [nmi, ami]

    # print "accuracy: ", metrics.adjusted_rand_score(labels, k_means_labels)
    # metrics.homogeneity_score(labels, labels_pred)
    # metrics.completeness_score(labels, labels_pred)
    # metrics.silhouette_score(X, labels, metric='euclidean')




def spectral_clustering(features, labels = [], n_clusters = 10, similarity = None, visual = False, n_neighbors= 10):
    '''

    :param features:
    :param labels:
    :param similarity: type of similarity matrix will be used
        {None(default to RBF kernel), knn/nearest_neighbors, precomputed}
    :param visual
    :return:
    '''

    # --------- translate user input method to spectral clustering input
    if similarity is None:
    #     default to covariance
        simMatrix = cdist(features, features, 'correlation')
        similarity = 'precomputed'
        spectral= SpectralClustering(n_clusters = n_clusters, random_state=1, affinity = similarity)
    elif similarity == 'knn' or similarity == 'nearest_neighbors':
        similarity = 'nearest_neighbors'
        simMatrix = features
        spectral = SpectralClustering(n_clusters=n_clusters, n_neighbors=n_neighbors,
                                      random_state=1, affinity=similarity)
    else:
        # todo: rank order distance to get similarity matrix
        similarity = 'precomputed'
        simMatrix = features
        spectral = SpectralClustering(n_clusters=n_clusters,
                                      random_state=1, affinity=similarity)

    t0 = time.time()

    t_batch = time.time() - t0

    spectral.fit(simMatrix)
    spectral_labels = spectral.labels_

    if visual:
        fig = pt.figure()
        ax = fig.add_subplot(111)
        color = cm.rainbow(np.linspace(0, 1, n_clusters))
        inertia = 0

        for k, col in zip(range(n_clusters), color):
            my_members = spectral_labels == k
            ax.plot(features[my_members, 0], features[my_members, 1], 'w',
                    markerfacecolor=col, marker='o')
            inertia += np.array([features[my_members, 0], features[my_members, 1]]).var()

        title = 'Spectral clustering ' + (similarity if similarity else '')
        ax.set_title(title)
        ax.set_xticks(())
        ax.set_yticks(())
        pt.axis()
        pt.text(-2.5,2, 'train time: %.2fs\ninertia: %f' % (
            t_batch, inertia))

    nmi = metrics.normalized_mutual_info_score(labels, spectral_labels)
    ami = metrics.adjusted_mutual_info_score(labels, spectral_labels)

    print "mutual information score: adjusted - %0.8f, normalized - %0.8f" %(ami, nmi)
    return [nmi, ami]

    # print "accuracy: ", metrics.adjusted_rand_score(labels, spectral_labels)
    # unique_clusters = np.unique(labels)
    # cluster_map = {}
    #
    # for cl_i in unique_clusters:
    #     sp_i = spectral_labels[np.where(labels==cl_i)[0]]
    #     cluster_map[cl_i]


# todo: implement affinity propagation
def ap_clustering(features, labels = [], affinity = None, preference = None, visual = False):
    '''

    :param features:
    :param labels:
    :param affinity:{'euclidean', 'correlation', }
    :param preference default -200
    :param visual
    :return: nmi, ami
    '''
    # clusters = np.unique(labels)
    # n_clusters = len(clusters)

    ap = AffinityPropagation()

    if affinity is 'correlation':
        # correlation
        simMatrix = cdist(features, features, 'correlation')
        affinity = 'precomputed'
        ap = AffinityPropagation(affinity=affinity, max_iter=100)
        ap_labels = ap.fit_predict(features)

    elif affinity is 'square_correlation':
        corrG = corrcoef(features)
        ap_labels = ap.fit_predict(corrG*corrG)

    else:
        simMatrix = features
        affinity = 'euclidean'
        ap = AffinityPropagation(affinity=affinity, max_iter=100)
        ap_labels = ap.fit_predict(features)

    if visual:
        fig = pt.figure()
        ax = fig.add_subplot(111)
        color = cm.rainbow(np.linspace(0, 1, n_clusters))
        inertia = 0


        for k, col in zip(range(n_clusters), color):
            my_members = ap_labels == k
            ax.plot(features[my_members, 0], features[my_members, 1], 'w',
                    markerfacecolor=col, marker='o')
            inertia += np.array([features[my_members, 0], features[my_members, 1]]).var()

        title = 'AP clustering '
        ax.set_title(title)
        ax.set_xticks(())
        ax.set_yticks(())
        pt.axis()
        # pt.text(-2.5,2, 'train time: %.2fs\ninertia: %f' % (t_batch, inertia))

    nmi = metrics.normalized_mutual_info_score(labels, ap_labels)
    ami = metrics.adjusted_mutual_info_score(labels, ap_labels)

    n_cluster = np.unique(ap_labels).shape[0]

    print "mutual information score: n_cluster - %d, normalized - %0.8f" %(n_cluster, nmi)
    return [nmi, n_cluster]


def select_dimension(model):
    '''
    chose two most siginificant dimension, return features in array
    :param model: original model
    :return: two most significant features, np.array
    '''

    topTwo = np.array(model.get_most_significant_input_dimensions()[:2])
    feat0 = np.array(model.latent_mean.values[:,topTwo[0]])
    feat1 = model.latent_mean.values[:,topTwo[1]]
    selectFeat = np.vstack((feat0, feat1))

    return  np.transpose(selectFeat)

#
# # run clustering as it self
# if __name__ == "__main__":
#     if len(sys.argv) < 2:
#         print 'cluster_example.py <inputfile>'
#         sys.exit(2)
#
#     parse_result(sys.argv[1:])