from __future__ import print_function

'''
%load_ext autoreload
%autoreload 2

to cluster openface features
read in file containing the models and run clustering and save in csv file

'''

from dgplvm_eval import *
from process_data import *
from cluster_example import *

import pickle
import pandas as pd
from copy import deepcopy

import numpy as np
from dgplvm_eval import *
from cluster_example import *


import pickle
# read in models


# ---- ---- ---- ---- ---- ----- ----- ------------ file start here
# ---- ---- ---- ----- get face data




# #  toy
# sigmaOption = [np.power(10.0,j) for j in [-7, -4]]
# dimOption = range(len(which) - 1, len(which)+1)
#
# clusterOption = range(len(which) / 2, len(which)+1)
# knnOption = [8,10,12]

# ---- ---- ---- ----- cluster function
def get_clustering(argvs):

    modelParamList = []
    clusterParamList = []
    nmiList = []
    amiList = []

    # todo modify the two cases
    if len(argvs) == 2:
        infile = argvs[0]
        outfile = argvs[1]

        file = open(infile, 'rb')
        models = pickle.load(file)
        file.close()

        if hasattr(models[0], 'labels'):
            labels = models[0].labels
            featMat = models[0].latent_mean
            which = np.unique(labels)
        else:
            filename = '../features/openface_files.txt'
            featMat, labels, which = prepare_face(filename)

        # ---- ---- ---- ----- selection of parameters
        sigmaOption = [np.power(10.0, j) for j in [-7, -4, -1, 1, 4]]
        # dimOption = range(len(which) - 1, len(which) + len(which) / 2)

        clusterOption = range(len(which) / 2, len(which) * 2)
        knnOption = [8, 10, 12, 15]

        # ---- ---- ---- ---- model parameter
        for i in range(len(models)):
            modelParam = []
            m = models[i]
            priorName = m.prior_name

            # name
            if priorName == 'None':
                modelParam.append('GPLVM')
            else:
                modelParam.append(priorName)

            # latent dim
            modelParam.append(m.latent_mean.shape[1])
            # sigma
            if priorName == 'Gaussian':
                sigma = sigmaOption[(i-2)%len(sigmaOption)]
                modelParam.append(sigma)

            # ---- ----- ----- ----- clustering for each model
            print (" ----- running clustering  ------")
            print (modelParam)

            print(" ----- running affinity propagation with l2 distance")
            ares = ap_clustering(m.latent_mean, labels=labels, visual=False)
            #
            # print(" ----- running affinity propagation with squared correlation")
            # ares = ap_clustering(m.latent_mean, labels=labels, affinity='square_correlation', visual=False)
            # print apNMI
            modelParamList.append(modelParam)
            clusterParam = ['ap_l2']
            clusterParamList.append(clusterParam)
            nmiList.append(ares[0])
            amiList.append(ares[1])


            print(" ----- running affinity propagation with squared correlation")
            ares = ap_clustering(m.latent_mean, labels=labels, affinity='square_correlation', visual=False)
            # print apNMI
            modelParamList.append(modelParam)
            clusterParam = ['ap_cov']
            clusterParamList.append(clusterParam)
            nmiList.append(ares[0])
            amiList.append(ares[1])


            for nn in clusterOption:
                print (" ----- running K mean clustering, dimension = %d" % (nn))
                kres = kmean_clustering(m.latent_mean, labels, n_clusters=nn)
                # print kNMI
                # clusterList.append(np.asarray([nmi, ami, 'kmean']))
                modelParamList.append(modelParam)
                clusterParam = ['kmean', nn]
                clusterParamList.append(clusterParam)
                nmiList.append(kres[0])
                amiList.append(kres[1])

                for kk in knnOption:
                    print(" ----- running spectral clustering with nearsest neighbor, dimension = %d, neighbors = %d" % (
                    nn, kk))
                    sres = spectral_clustering(m.latent_mean, labels, n_clusters=nn, n_neighbors=kk, similarity='knn',visual=False)
                    modelParamList.append(modelParam)
                    clusterParam = ['spectral', nn, kk]
                    clusterParamList.append(clusterParam)
                    nmiList.append(sres[0])
                    amiList.append(sres[1])

    # ------ ----- ------ without model
    else:
        filename = '../features/openface_files.txt'
        featMat, labels, which = prepare_face(filename)

        # ---- ---- ---- ----- selection of parameters
        sigmaOption = [np.power(10.0, j) for j in [-7, -4, -1, 1, 4]]
        # dimOption = range(len(which) - 1, len(which) + len(which) / 2)

        clusterOption = range(len(which) / 2, len(which) * 2)
        knnOption = [8, 10, 12, 15]

        outfile = argvs[0]
        # -------- --------- clustering
        print (" ----- running clustering  ------")
        # max_iters  = 0
        print(" ----- running affinity propagation with squared correlation")
        ares = ap_clustering(featMat, labels=labels, affinity='square_correlation', visual=False)
        # print apNMI
        clusterParam = ['ap']
        clusterParamList.append(clusterParam)
        nmiList.append(ares[0])
        amiList.append(ares[1])

        print(" ----- running affinity propagation with l2 distance")
        ares = ap_clustering(featMat, labels=labels, visual=False)
        #
        # print(" ----- running affinity propagation with squared correlation")
        # ares = ap_clustering(m.latent_mean, labels=labels, affinity='square_correlation', visual=False)
        # print apNMI
        modelParamList.append(clusterParam)
        clusterParam = ['ap_l2']
        clusterParamList.append(clusterParam)
        nmiList.append(ares[0])
        amiList.append(ares[1])

        for nn in clusterOption:
            param = "n_cluster = %d" % nn
            print (" ----- running K mean clustering, dimension = %d" %(nn))
            kres = kmean_clustering(featMat, labels, n_clusters= nn)
            # print kNMI
            # clusterList.append(np.asarray([nmi, ami, 'kmean']))
            clusterParamList.append(['kmean', nn])
            nmiList.append(kres[0])
            amiList.append(kres[1])

            for kk in knnOption:
                print (" ----- running spectral clustering with nearsest neighbor, dimension = %d, neighbors = %d" % (nn, kk))
                sres = spectral_clustering(featMat, labels, n_clusters=nn, n_neighbors=kk, similarity='knn', visual=False)
                clusterParamList.append(['spectral', nn, kk])
                nmiList.append(sres[0])
                amiList.append(sres[1])

        modelParamList = clusterParamList
        # print sNMI
        # clusterList.append(np.asarray([nmi, ami, 'spectral knn']))
        # nmi, ami = spectral_clustering(m.latent_mean, labels, n_clusters=nn, visual=False)
        # clusterList.append(np.asarray([nmi, ami, 'spectral cdist'])


    # ---- ----- ----- ---- ---- save to files

    # save to csv
    evalDict = {'model params': modelParamList, 'cluster param': clusterParamList, 'nmi': nmiList, 'ami': amiList}
    # fo = 'digits_nmi_%diter.npy' % max_iters
    # np.save(fo, evalDict)

    df = pd.DataFrame(evalDict)
    df.to_csv(outfile, encoding='utf-8')


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print ('run_clustering.py (optional: <inputfile>) <outputfile.csv>')
        sys.exit(2)

    # else:
    #     print (len(sys.argv[1:]))
    #
    get_clustering(sys.argv[1:])