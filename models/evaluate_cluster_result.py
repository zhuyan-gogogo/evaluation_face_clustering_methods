from process_data import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd


filename = 'digits_model_1500i.csv'
df = pd.read_csv(filename)

df['modelName'] = df['model params'].apply(lambda x: x.split('[')[1].split(']')[0].split(',')[0])
df['modelDim'] = df['model params'].apply(lambda x: int(x.split('[')[1].split(']')[0].split(',')[1]))
df['modelVar'] = df['model params'].apply(lambda x:
                                    0.0 if len(x.split('[')[1].split(']')[0].split(',')) < 3
                                    else x.split('[')[1].split(']')[0].split(',')[2])
df['clusterName'] = df['cluster param'].apply(lambda x: x.split('[')[1].split(']')[0].split(',')[0])
df['clusterDim'] = df['cluster param'].apply((lambda x:
                                    0 if len(x.split('[')[1].split(']')[0].split(',')) < 2
                                    else x.split('[')[1].split(']')[0].split(',')[1]))

groupModel = list(df.groupby('modelName'))
gplvmModel = groupModel[0]
uniformModel = groupModel[1]
gaussianModel = groupModel[2]

clusterMethod = list(df.groupby('clusterName'))
apCluster = clusterMethod[0]

nmiList = df['nmi']
amiList = df['ami']


# hypothesis: gplvm, dim -> nmi

g = sns.lmplot(x="modelDim", y="nmi", hue="clusterName", groupby = 'modelName')
g = sns.lmplot(x="modelDim", y="nmi", hue="clusterName", data=groupModel[0].count())

grid = sns.FacetGrid(df, col='modelName', size=2.2, aspect=1.6)
grid.map(sns.boxplot, 'modelDim', 'nmi', palette='deep')

grid = sns.FacetGrid(df, row = 'clusterName',col='modelName', size=2.2, aspect=1.6)
grid.map(sns.boxplot, 'modelDim', 'nmi', palette='deep')

sns.plt.show()