'''
%load_ext autoreload
%autoreload 2
'''

from dgplvm_eval import *
from process_data import *
from cluster_example import *

import pickle
import pandas as pd
import copy

#get features
#filename = 'feature_path.txt'
#faceVec, labels = face_pixels(filename, down=0.5)
'''
to build feature dictionary properly, the file containing the paths to mat file should be put in root directory
eg.
features/openface_files.txt
features/openface/*.mat
'''

filename = '../features/openface_files.txt'
faceFeat = build_dict(filename)
labels = np.array([[key] * faceFeat[key].shape[1] for key in faceFeat])
which = faceFeat.keys()
featMat = [faceFeat[key].T for key in faceFeat]
featMat = np.vstack(tuple(featMat))

eachsize = [faceFeat[key].shape[1] for key in faceFeat]
labels = np.array([[str(key)] * faceFeat[key].shape[1] for key in faceFeat])

merge = []
for i in labels:
    merge = merge + i

labels = np.asarray(merge)

# to select
# test
sigmaOption = [np.power(10.0,j) for j in [-7, -4]]
dimOption = range(len(which) - 1, len(which)+1) #2
clusterOption = range(len(which) / 2, len(which) +1) #
knnOption = [8,10,12]

# sigmaOption = [np.power(10.0,j) for j in [-7, -4, -1, 1, 4]]
# dimOption = range(len(which) - 1, len(which)+len(which)/2)
# clusterOption = range(len(which) / 2, len(which) * 2)
# knnOption = [8,10,12,15]

ss, dd = np.meshgrid(sigmaOption, dimOption)
ss = ss.flatten()
dd = dd.flatten()

max_iters = 1

# ------- -------- -------- protected
rootParamList = []
secParamList = []
nmiList = []
amiList = []
mList = []

for n in dimOption:
    # list of models, newed for clustering
    sub = []
    headParam =[]
    param = "input_dim = %d" % (n)
    print (" ----- running GPLVM " + param)
    m1 = build_model(featMat, labels, input_dim = n, visual=False, max_iters=max_iters, model='gplvm')
    mList.append(copy.deepcopy(m1))
    sub.append(copy.deepcopy(m1))
    headParam.append(['GPLVM', n, 0.0])
    # headParam.append(param)

    param = "input_dim = %d" % (n)
    print (" ----- running DGPLVM uniform prior: " + param)
    m2 = build_model(featMat, labels, input_dim=n, visual=False, max_iters=max_iters, model='uniform')
    mList.append(copy.deepcopy(m2))
    sub.append(copy.deepcopy(m2))
    headParam.append(['uniform', n, 0.0])
    # headParam.append(param)

    for s in sigmaOption:
        param = "sigma2 = %.8f; input_dim = %d" % (s, n)
        print (" ----- running DGPLVM gaussian prior: " + param)
        m3 = build_model(featMat, labels, sigma2=s, input_dim=n, visual=False, max_iters=max_iters, model='gaussian')
        mList.append(copy.deepcopy(m3))
        sub.append(copy.deepcopy(m3))
        headParam.append(['gaussian', n, s])
        # headParam.append(param)

        del m3

    # -------- --------- clustering

    print " ----- running clustering  ------"

    for nn in clusterOption:
        param = "n_cluster = %d" % nn
        print (" ----- running K mean clustering, dimension = %d" %(nn))
        kres = [kmean_clustering(m.latent_mean, labels, n_clusters= nn) for m in sub]
        # print kNMI
        # clusterList.append(np.asarray([nmi, ami, 'kmean']))
        rootParamList += headParam
        subParam = [['kmean', nn] for p in headParam]
        secParamList += subParam
        nmiList += [k[0] for k in kres]
        amiList += [k[1] for k in kres]

        for kk in knnOption:
            print (" ----- running spectral clustering with nearsest neighbor, dimension = %d, neighbors = %d" % (nn, kk))
            sres = [spectral_clustering(m.latent_mean, labels, n_clusters=nn, n_neighbors=kk, similarity='knn', visual=False) for m in sub]
            rootParamList += headParam
            subParam = [['spectral', nn, kk] for p in headParam]
            secParamList += subParam
            nmiList + [k[0] for k in sres]
            amiList += [k[1] for k in sres]

        # print sNMI
        # clusterList.append(np.asarray([nmi, ami, 'spectral knn']))
        # nmi, ami = spectral_clustering(m.latent_mean, labels, n_clusters=nn, visual=False)
        # clusterList.append(np.asarray([nmi, ami, 'spectral cdist']))

        print (" ----- running affinity propagation with squared correlation")
        ares = [ap_clustering(m.latent_mean, labels=labels, affinity='square_correlation', visual=False) for m in sub]
        # print apNMI
        rootParamList += headParam
        subParam = [['ap', nn] for p in headParam]
        secParamList += subParam
        nmiList.append([k[0] for k in ares])
        amiList.append([k[1] for k in ares])


# ------ ----- ------ without model
#
#     # -------- --------- clustering
#
# print " ----- running clustering  ------"
# model = 0
#
# for nn in clusterOption:
#     param = "n_cluster = %d" % nn
#     print (" ----- running K mean clustering, dimension = %d" %(nn))
#     kres = kmean_clustering(featMat, labels, n_clusters= nn)
#     # print kNMI
#     # clusterList.append(np.asarray([nmi, ami, 'kmean']))
#     secParamList.append(['kmean', nn])
#     nmiList.append(kres[0])
#     amiList.append(kres[1])
#
#     for kk in knnOption:
#         print (" ----- running spectral clustering with nearsest neighbor, dimension = %d, neighbors = %d" % (nn, kk))
#         sres = spectral_clustering(featMat, labels, n_clusters=nn, n_neighbors=kk, similarity='knn', visual=False)
#         secParamList.append(['spectral', nn, kk])
#         nmiList.append(sres[0])
#         amiList.append(sres[1])
#
#     # print sNMI
#     # clusterList.append(np.asarray([nmi, ami, 'spectral knn']))
#     # nmi, ami = spectral_clustering(m.latent_mean, labels, n_clusters=nn, visual=False)
#     # clusterList.append(np.asarray([nmi, ami, 'spectral cdist']))
#
#     print (" ----- running affinity propagation with squared correlation")
#     ares = ap_clustering(featMat, labels=labels, affinity='square_correlation', visual=False)
#     # print apNMI
#     secParamList.append(['ap', nn])
#     nmiList.append(kres[0])
#     amiList.append(kres[1])
#
# rootParamList = secParamList

# ---- ----- ----- ---- ---- save to files
PIK = '../experiments/openface_models_%diter_re.dat' % max_iters
with open(PIK, "wb") as f:
    pickle.dump(mList, f)

# with open(PIK, "rb") as f:
#     print pickle.load(f)

# save to csv
evalDict = {'model params': rootParamList, 'cluster param': secParamList, 'nmi': nmiList, 'ami': amiList}
# fo = 'digits_nmi_%diter.npy' % max_iters
# np.save(fo, evalDict)

df = pd.DataFrame(evalDict)
CSV = '../experiments/openface_models_%diter_re.csv' % max_iters
df.to_csv(CSV, encoding='utf-8')

