'''
%load_ext autoreload
%autoreload 2
'''

from dgplvm_eval import *
from process_data import *
from cluster_example import *

import pickle
import pandas as pd
from copy import deepcopy

import numpy as np
from dgplvm_eval import *
from cluster_example import *

# df.set_index(['Form', 'nr Element', 'Type', 'Test']) will combine same entry

Y, labels, which = prepare_digits('../features/digits.npy')
clusterOption = range(len(which) / 2, len(which)+1)

print (" ----- running clustering  ------")
# print (modelParam)
abest = 0
kbest = 0
sbest = 0

print(" ----- running affinity propagation with squared correlation")
# ares = ap_clustering(Y, labels=labels, visual=False)
ares = ap_clustering(Y, labels=labels, affinity='square_correlation', visual=False)
# print apNMI
# modelParamList.append(modelParam)
# clusterParam = ['ap']
# clusterParamList.append(clusterParam)
# nmiList.append(ares[0])
# amiList.append(ares[1])
if ares[0]>abest:
    abest = ares[0]


for nn in clusterOption:
    print (" ----- running K mean clustering, dimension = %d" % (nn))
    kres = kmean_clustering(Y, labels, n_clusters=nn)
    # print kNMI
    # clusterList.append(np.asarray([nmi, ami, 'kmean']))
    # modelParamList.append(modelParam)
    # clusterParam = ['kmean', nn]
    # clusterParamList.append(clusterParam)
    # nmiList.append(kres[0])
    # amiList.append(kres[1])
    if kres[0] > kbest:
        kbest = kres[0]

    print(" ----- running spectral clustering with nearsest neighbor, dimension = %d" % (
        nn))
    sres = spectral_clustering(Y, labels, n_clusters=nn, similarity='knn', visual=False)
    # modelParamList.append(modelParam)
    # clusterParam = ['spectral', nn]
    # clusterParamList.append(clusterParam)
    # nmiList.append(sres[0])
    # amiList.append(sres[1])
    if sres[0] > sbest:
        sbest = sres[0]
